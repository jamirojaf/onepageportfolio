# VueJS portfolio template

## Versions

installeer, serve en build met node versie 14.19.0

## Project setup

``` sh
git clone
npm install
```

### Compiles and hot-reloads for development

``` sh
npm run serve
```

### Compiles and minifies for production

``` sh
npm run build
```

### Lints and fixes files

``` sh
npm run lint
```
